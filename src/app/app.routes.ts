import { UserFormDataComponent } from './page/user-form-data/user-form-data.component';

import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';

// USER DEFINED ROUTES
import { PageComponent } from './page/page-container/page.component';
import { UserFormComponent } from './page/user-form/user-form.component';
import { UnauthorizedComponent } from './unauthorized/unauthorized.component';

// ROUTING
const appRoutes: Routes = [
    {
        path: '',
        redirectTo: 'page',
        pathMatch: 'full'
    },
    { path: 'app', component: AppComponent },
    {
        path: 'page', component: PageComponent,
        children: [
            { path: '', redirectTo: 'user-form', pathMatch: 'full' },
            { path: 'user-form', component: UserFormComponent },
            { path: 'userformdata', component: UserFormDataComponent }

        ]
    },


]



export const routing = RouterModule.forRoot(appRoutes);