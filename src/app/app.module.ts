import { PageService} from './page/page-service/page-service.service';
import { routing } from './app.routes';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { UnauthorizedComponent } from './unauthorized/unauthorized.component';
import { PageComponent } from './page/page-container/page.component';
import { UserFormComponent } from './page/user-form/user-form.component';
import { HeaderComponent } from './page/header/header.component';
import { FooterComponent } from './page/footer/footer.component';
import { UserFormDataComponent } from './page/user-form-data/user-form-data.component';


@NgModule({
  declarations: [
    AppComponent,
    UnauthorizedComponent,
    PageComponent,
    UserFormComponent,
    HeaderComponent,
    FooterComponent,
    UserFormDataComponent
  ],
  imports: [
    BrowserModule,
    routing,
    ReactiveFormsModule
  ],
  providers: [PageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
