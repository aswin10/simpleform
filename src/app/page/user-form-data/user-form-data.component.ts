import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user-form-data',
  templateUrl: './user-form-data.component.html',
  styleUrls: ['./user-form-data.component.css']
})
export class UserFormDataComponent implements OnInit {

  userFormData
  constructor() {
    let formData = sessionStorage.getItem('userForm')
    this.userFormData = JSON.parse(formData)
   }

  ngOnInit() {
  }

}
