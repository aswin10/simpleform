import { Injectable } from '@angular/core';
import { FormControl, AbstractControl } from '@angular/forms';

@Injectable()
export class PageService {


  public vEmailPattern = '([a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]{1}[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]*)((@[a-zA-Z\-]{2}[a-zA-Z\-]*)[\\\.](([a-zA-Z]{3}|[a-zA-Z]{2})|([a-zA-Z]{3}|[a-zA-Z]{2}).[a-zA-Z]{2}))'; // Applicable for email field
  public vPasswordPattern = /^(?=.*[a-zA-Z])(?=.*[!@#$%^&*])[\w!@#$%^&*]{2,}$/;
  constructor() {

  }


  /******************************************************
    * CHECK CONTROLS
    ******************************************************/
  markAllDirty(control: AbstractControl) {
    let opts: {
      onlySelf?: true;
    }
    if (control.hasOwnProperty('controls')) {
      control.markAsDirty(opts) // mark group
      let ctrl = <any>control;
      for (let inner in ctrl.controls) {
        this.markAllDirty(ctrl.controls[inner] as AbstractControl);
      }
    }
    else {
      (<FormControl>(control)).updateValueAndValidity();
      (<FormControl>(control)).markAsDirty(opts);
    }
  }


  /******************************************************
  * FORM VALIDATION,
  * MONITORS VALUE CHANGES IN FORM
  ******************************************************/
validationMessages = {
  "email":{
    "required":"Email is required",
    "pattern":"Enter a valid email"
  },
   "password":{
    "required":"Password is required",
    "minlength":"Password should be 8 characters long.",
    "pattern":"Password should contain atleast one character and one special character"
  }
}
  // ACCESSABLE VARIABLES
  
  onValueChanged(myForm?, data?: any, formErrors?: any, ) {
    if (!myForm) { return; }
    // // console.log(formErrors)
    const form = myForm;
    for (const field in formErrors) {

      formErrors[field] = '';
      const control = form.get(field);
      if (control && control.dirty && !control.valid) {

        const messages = this.validationMessages[field];
        // // console.log(messages)
        for (const key in control.errors) {

          formErrors[field] += messages[key] + ' ';
        }
      }


    }
  }

}
