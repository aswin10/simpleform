import { PageService } from './../page-service/page-service.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, } from "@angular/forms";
import { Router } from "@angular/router";
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';


@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})


export class UserFormComponent implements OnInit {

  userForm: FormGroup
  userFormErrors = {
    'email': '',
    'password': ''
  }
  levels = ["Basic", "Advanced", "Pro"]
  constructor(public fb: FormBuilder,
    public pageService: PageService,
    public router: Router) {

  }

  ngOnInit() {
    this.formInit()
    this.userForm.valueChanges
      .debounceTime(1000)
      .distinctUntilChanged()
      .subscribe(data => {
        // // console.log('data',data)
        this.pageService.onValueChanged(this.userForm, data, this.userFormErrors)
      });
    this.pageService.onValueChanged();

  }

  formInit(){
     this.userForm = this.fb.group({
      email: ['', [Validators.required, Validators.pattern(this.pageService.vEmailPattern)]],
      password: ['', [Validators.required, Validators.minLength(8), Validators.pattern(this.pageService.vPasswordPattern)]],
      subscription: ['Advanced']

    })
  }

  submitForm() {
    if (!this.userForm.valid)
      this.pageService.markAllDirty(this.userForm)
    else
      this.router.navigate(['/page/userformdata'])
      sessionStorage.setItem('userForm',JSON.stringify(this.userForm.value))
  }


  clearForm() {
    this.userForm.reset()
    this.formInit()

  }
}
